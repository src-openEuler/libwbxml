%undefine __cmake_in_source_build

Name:                libwbxml
Version:             0.11.10
Release:             2
Summary:             Library and tools to parse, encode and handle WBXML documents
License:             LGPL-2.1-or-later
URL:                 https://github.com/%{name}/%{name}
Source:              %{url}/archive/%{name}-%{version}.tar.gz
# Fix installing CMake configuration files, in upstream after 0.11.10,
# <https://github.com/libwbxml/libwbxml/pull/95>.
Patch0:              libwbxml-0.11.10-Fix-installing-CMake-configuration-files.patch
BuildRequires:       cmake >= 2.4 coreutils expat-devel gcc make pkgconfig(check) perl-interpreter
Obsoletes:           wbxml2 <= 0.9.3

%description
The WBXML Library (libwbxml) contains a library and its associated tools to
parse, encode and handle WBXML documents. The WBXML format is a binary
representation of XML, defined by the Wap Forum, and used to reduce
bandwidth in mobile communications.

%package devel
Summary:             Development files of %{name}
Requires:            %{name}%{?_isa} = %{version}-%{release}
Provides:            wbxml2-devel = %{version}-%{release}
Obsoletes:           wbxml2-devel < 0.9.3
%description devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%prep
%autosetup -p1 -n %{name}-%{name}-%{version}

%build
%cmake \
    -DBUILD_SHARED_LIBS:BOOL=ON \
    -DBUILD_STATIC_LIBS:BOOL=OFF \
    -DENABLE_INSTALL_DOC:BOOL=OFF \
    -DENABLE_UNIT_TEST:BOOL=ON \
    -DWBXML_ENCODER_USE_STRTBL:BOOL=ON \
    -DWBXML_INSTALL_FULL_HEADERS:BOOL=OFF \
    -DWBXML_LIB_VERBOSE:BOOL=OFF \
    -DWBXML_SUPPORT_AIRSYNC:BOOL=ON \
    -DWBXML_SUPPORT_CO:BOOL=ON \
    -DWBXML_SUPPORT_CONML=ON \
    -DWBXML_SUPPORT_DRMREL:BOOL=ON \
    -DWBXML_SUPPORT_EMN:BOOL=ON \
    -DWBXML_SUPPORT_OTA_SETTINGS:BOOL=ON \
    -DWBXML_SUPPORT_PROV:BOOL=ON \
    -DWBXML_SUPPORT_SI:BOOL=ON \
    -DWBXML_SUPPORT_SL:BOOL=ON \
    -DWBXML_SUPPORT_SYNCML:BOOL=ON \
    -DWBXML_SUPPORT_WML:BOOL=ON \
    -DWBXML_SUPPORT_WTA:BOOL=ON \
    -DWBXML_SUPPORT_WV:BOOL=ON \
%{cmake_build}

%install
%cmake_install

%check
%ctest

%files
%license COPYING GNU-LGPL
%doc BUGS ChangeLog README References THANKS TODO
%{_bindir}/*
%{_libdir}/libwbxml2.so.*

%files devel
%{_includedir}/*
%{_libdir}/libwbxml2.so
%{_libdir}/cmake/libwbxml2
%{_libdir}/pkgconfig/libwbxml2.pc

%changelog
* Tue Nov 05 2024 Funda Wang <fundawang@yeah.net> - 0.11.10-2
- adopt to new cmake macro

* Tue Jul 30 2024 Ge Wang <wang__ge@126.com> - 0.11.10-1
- Upgrade package t0 0.11.10

* Tue Apr 11 2023 xu_ping <707078654@qq.com> - 0.11.8-1
- Upgrade package to 0.11.8

* Thu Jul 23 2020 wangyue <wangyue92@huawei.com> - 0.11.7-1
- package init
